from django.test import TestCase, SimpleTestCase
from django.test.client import RequestFactory
from HTMLParser import HTMLParseError
from django.test.html import parse_html
from django.template import Context, Template
from django.core.urlresolvers import reverse
from model_factories import PollFactory, ChoiceFactory
from django.utils import timezone
import datetime
from django.forms.widgets import HiddenInput
from polls.models import Poll, Choice
from polls.forms import ChoiceInlineFormSet


def clear_db():
    Poll.objects.all().delete()  # Clear database


def _poll_url(pk, url='detail'):
    """
    Returns the respective url for a given poll pk
    """
    return reverse('polls:{0}'.format(url), kwargs={'poll_id': pk})


def _build_edit_post_data(data={}, total_forms=0, initial_forms=0,
                          max_num_forms=0):
    """
    Returns a dict with all required post data by edit poll view
    """
    formset_metadata = {
        'choice_set-TOTAL_FORMS': total_forms,
        'choice_set-INITIAL_FORMS': initial_forms,
        'choice_set-MAX_NUM_FORMS': max_num_forms
    }
    formset_metadata.update(data)
    return formset_metadata


def _build_edit_choice_post_data(choices):
    """
    Recieves a list of choices and returns
    a dict with the required post data for each
    """
    data = {}
    index = 0
    for choice in choices:
        data.update({
            'choice_set-{0}-id'.format(index): str(choice.pk),
            'choice_set-{0}-poll'.format(index): str(choice.poll.pk),
            'choice_set-{0}-choice'.format(index): choice.choice
        })
        index += 1
    return data


class PollsModelTest(TestCase):

    def test_was_published_recently(self):
        p = PollFactory.build()
        self.assertTrue(p.was_published_recently())

        today = timezone.now() - datetime.timedelta(hours=23, seconds=59)
        p = PollFactory.build(pub_date=today)
        self.assertTrue(p.was_published_recently())

    def test_was_not_published_recently(self):
        yesterday = timezone.now() - datetime.timedelta(days=1, seconds=1)
        p = PollFactory.build(pub_date=yesterday)
        self.assertFalse(p.was_published_recently())

    def test_default_total_votes_count(self):
        p = PollFactory.build()
        self.assertEqual(p.total_votes_count, 0)

    def test_total_votes_count(self):
        p = PollFactory()
        ChoiceFactory(poll=p, votes=1)
        ChoiceFactory(poll=p, votes=1)
        ChoiceFactory(poll=p, votes=0)
        self.assertEqual(p.total_votes_count, 2)

    def tearDown(self):
        clear_db()


class ChoiceModelTest(TestCase):

    def test_default_votes_percentage(self):
        c = ChoiceFactory()
        self.assertEqual(c.votes_percentage, 0)

    def test_votes_percentage(self):
        p = PollFactory()
        c = ChoiceFactory(poll=p, votes=1)
        ChoiceFactory(poll=p, votes=1)
        ChoiceFactory(poll=p, votes=0)
        self.assertEqual(c.votes_percentage, 50)

    def test_votes_percentage_zero_votes(self):
        """
        votes_percentage should return 0 if the respective poll
        has no votes yet
        """
        p = PollFactory()
        c = ChoiceFactory(poll=p, votes=0)
        ChoiceFactory(poll=p, votes=0)
        ChoiceFactory(poll=p, votes=0)
        self.assertEqual(c.votes_percentage, 0)

    def tearDown(self):
        clear_db()


class PollsViewsTest(TestCase):
    def setUp(self):
        # Insert some data to work with
        self.polls = [PollFactory() for i in range(10)]
        for poll in self.polls:
            [ChoiceFactory(poll=poll, votes=i * i) for i in range(3)]

    def test_empty_index_view(self):
        clear_db()
        url = reverse('polls:index')
        resp = self.client.get(url)
        self.assertContains(resp, 'Sorry, no polls found.')

    def test_index_view(self):
        url = reverse('polls:index')
        resp = self.client.get(url)

        # Check that the polls index returns http 200
        self.assertEqual(resp.status_code, 200)

        # Check for the correct context variables
        self.assertTrue('polls_list' in resp.context)
        polls = [k in self.polls[:5] for k in resp.context['polls_list']]
        self.assertTrue(all(polls))

    def _get_nonexistant_poll_id(self):
        return Poll.objects.all().order_by('-pk')[0].pk + 1

    def _check_404(self, url, method='get'):
        """
        Asserts that a given url and method returns 404 status code
        """
        m = getattr(self.client, method)
        if not m:
            self.fail('Called invalid method {0} on client'.format(method))
        resp = m(url)
        self.assertEquals(resp.status_code, 404)

    def test_get_detail_view(self):
        """
        Check that an existing poll detail view returns http 200
        """
        poll_id = self.polls[0].pk
        resp = self.client.get(_poll_url(pk=poll_id))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'polls/detail.html')
        self.assertEqual(resp.context['poll'].pk, poll_id)

    def test_invalid_get_detail_view(self):
        """
        Check that a not existing poll detail view returns http 404
        """
        non_existant_poll_id = self._get_nonexistant_poll_id()
        self._check_404(_poll_url(pk=non_existant_poll_id))

    def test_valid_post_detail_view_redirects(self):
        # Submit a post request and check it redirects to the same page
        poll_id = self.polls[0].pk
        voted_choice = self.polls[0].choice_set.all()[0]

        resp = self.client.post(_poll_url(pk=poll_id),
                                {'choice': voted_choice.pk})

        self.assertRedirects(resp, _poll_url(pk=poll_id))

    def test_post_detail_view(self):
        # Submit a post request and check it increments the choice vote counter
        poll_id = self.polls[0].pk
        voted_choice = self.polls[0].choice_set.all()[0]

        assert voted_choice.votes == 0  # this number will be incremented

        self.client.post(_poll_url(pk=poll_id),
                         {'choice': voted_choice.pk})
        self.assertEqual(Choice.objects.get(pk=voted_choice.pk).votes, 1)

    def test_invalid_post_detail_view(self):
        # Submit an invalid post request and check it increments
        # the choice vote counter and returns a redirect
        poll = self.polls[0]
        resp = self.client.post(_poll_url(pk=poll.pk))
        self.assertEqual(resp.status_code, 200)

        # All choices votes counters shouldn't have incremented
        choices = poll.choice_set.all()
        self.assertEqual(choices[0].votes, 0)
        self.assertEqual(choices[1].votes, 1)
        self.assertEqual(choices[2].votes, 4)

    def test_get_edit_view_status_code(self):
        poll_id = self.polls[0].pk
        resp = self.client.get(_poll_url(pk=poll_id, url='edit'))
        self.assertEqual(resp.status_code, 200)

    def test_get_edit_view_invalid_poll(self):
        non_existant_poll_id = self._get_nonexistant_poll_id()
        self._check_404(_poll_url(pk=non_existant_poll_id, url='edit'))

    def test_get_edit_view_templates_used(self):
        poll_id = self.polls[0].pk
        resp = self.client.get(_poll_url(pk=poll_id, url='edit'))
        self.assertTemplateUsed(resp, 'polls/edit.html')

    def test_get_edit_view_context(self):
        poll_id = self.polls[0].pk
        resp = self.client.get(_poll_url(pk=poll_id, url='edit'))
        self.assertTrue('formset' in resp.context)
        self.assertTrue('form' in resp.context)

    def test_post_edit_view_poll_question_updated(self):
        """
        Editing the poll's question should update the respective poll
        """
        poll_id = self.polls[0].pk
        post_data = _build_edit_post_data(data={'question':
                                                'edited question'})
        self.client.post(_poll_url(pk=poll_id, url='edit'), post_data)

        edited_poll = Poll.objects.get(pk=poll_id)
        self.assertEquals(edited_poll.question, 'edited question')

    def test_post_edit_view_choice_label_updated(self):
        """
        Editing the choice's label should update the respective choice
        """
        poll = self.polls[0]
        choice_id = poll.choice_set.all().order_by('pk')[0].pk

        assert Choice.objects.get(pk=choice_id).choice != 'new choice label'

        post_data = _build_edit_post_data(
            data={
                'question': poll.question,
                'choice_set-0-id': str(choice_id),
                'choice_set-0-poll': str(poll.pk),
                'choice_set-0-choice': 'new choice label'
            },
            total_forms=1, initial_forms=1, max_num_forms=1
        )
        self.client.post(_poll_url(pk=poll.pk, url='edit'), post_data)

        edited_choice = Choice.objects.get(pk=choice_id)
        self.assertEquals(edited_choice.choice, 'new choice label')

    def tearDown(self):
        clear_db()


class ChoiceEditInlineFormsetTest(SimpleTestCase):
    """
    Tests for the custom inline formset used for editing choices
    """
    def test_add_fields_widget(self):
        """
        The order integer field should be a hidden input
        """
        formset = ChoiceInlineFormSet()
        form = formset[0]
        self.assertIsInstance(form.fields['ORDER'].widget, HiddenInput)

    def test_get_queryset(self):
        """
        The queryset of the formset should return the
        choices ordered by '_order'
        """
        # Create a poll with 3 choices
        poll = PollFactory()
        [ChoiceFactory(poll=poll) for i in range(3)]

        # Retrieve the choice order
        order = poll.get_choice_order()

        # Reverse and update the choice order
        order.reverse()
        poll.set_choice_order(order)

        # Forms should now be retrieved in the new order (reversed)
        formset = ChoiceInlineFormSet(instance=poll)
        self.assertEquals(formset[0].instance.pk, order[0])
        self.assertEquals(formset[1].instance.pk, order[1])
        self.assertEquals(formset[2].instance.pk, order[2])

    def test_save_choices_order_unchanged(self):
        """
        Save method should not alter the choices order by default
        """
        # Create a poll with 3 choices
        poll = PollFactory()
        choices = [ChoiceFactory(poll=poll, _order=i) for i in range(3)]
        choice_order = poll.get_choice_order()

        # Get the required formset data
        data = {'question': poll.question}
        data.update(_build_edit_choice_post_data(choices))
        post_data = _build_edit_post_data(data=data, total_forms=len(choices),
                                          initial_forms=len(choices),
                                          max_num_forms=5)

        # Fake a request
        request = RequestFactory().post(_poll_url(poll.pk, url='edit'),
                                        post_data)

        # Save the formset
        formset = ChoiceInlineFormSet(request.POST, request.FILES,
                                      instance=poll)
        formset.save()

        self.assertEquals(choice_order,
                          Poll.objects.get(pk=poll.pk).get_choice_order())

    def test_save_choices_order_changed(self):
        """
        Save method should update the choices order if needed
        """
        # Create a poll with 3 choices
        poll = PollFactory()
        choices = [ChoiceFactory(poll=poll, _order=i) for i in range(3)]
        choice_order = poll.get_choice_order()

        # Get the required formset data but reverse the polls order
        choices.reverse()
        data = {'question': poll.question}
        data.update(_build_edit_choice_post_data(choices))
        post_data = _build_edit_post_data(data=data, total_forms=len(choices),
                                          initial_forms=len(choices),
                                          max_num_forms=5)

        # Fake a request
        request = RequestFactory().post(_poll_url(poll.pk, url='edit'),
                                        post_data)

        # Save the formset with the reversed poll order
        formset = ChoiceInlineFormSet(request.POST, request.FILES,
                                      instance=poll)
        formset.save()

        choice_order.reverse()
        self.assertEquals(choice_order,
                          Poll.objects.get(pk=poll.pk).get_choice_order())

    def tearDown(self):
        clear_db()


class PollTagsTest(TestCase):

    def _tag_test_helper(self, template, context, output):
        """
        Helper method that asserts that rendered template
        is equals to an expected output.
        """
        t = Template(template)
        c = Context(context)
        self.assertEqual(parse_html(t.render(c)), parse_html(output))

    def test_votes_percentage_bar(self):
        """
        A poll with one choice and one vote should render
        a progress bar with a 100% fill.
        """
        choice = ChoiceFactory(votes=1)

        template = """
        {% load poll_tags %}
        {% votes_percentage_bar choice_id %}"""

        context = {"choice_id": choice.pk}

        output = """
        <div class="progress">
            <div style="width: 100%;" class="bar"></div>
        </div>"""

        try:
            self._tag_test_helper(template, context, output)
        except HTMLParseError:
            self.fail('Generated HTML is invalid')

    def tearDown(self):
        clear_db()
