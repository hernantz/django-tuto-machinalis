import factory
from polls.models import Poll, Choice


class PollFactory(factory.Factory):
    FACTORY_FOR = Poll
    question = factory.Sequence(lambda n: 'Test question {0}?'.format(n))


class ChoiceFactory(factory.Factory):
    FACTORY_FOR = Choice
    poll = factory.SubFactory(PollFactory)
    choice = factory.Sequence(lambda n: 'Test choice {0}?'.format(n))
