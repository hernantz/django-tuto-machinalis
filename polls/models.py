from django.db import models
from django.utils import timezone
from django.db.models import Sum
import datetime


class Poll(models.Model):
    question = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Publication Date',
                                    default=timezone.now)

    def __unicode__(self):
        return self.question

    def was_published_recently(self):
        """
        Returns True or False depending on whether the poll
        was published in the last 24hs.
        """
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    was_published_recently.boolean = True
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.short_description = 'Was published recently?'

    @property
    def total_votes_count(self):
        """
        Returns the count of all votes associated to this poll
        """
        return self.choice_set.all().aggregate(Sum('votes'))[ 'votes__sum'] or 0


class Choice(models.Model):
    poll = models.ForeignKey(Poll)
    choice = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    class Meta:
        order_with_respect_to = 'poll'

    @property
    def votes_percentage(self):
        """
        Returns the percentage of votes this choice has, from the related poll
        """
        try:
            return (self.votes * 100) / self.poll.total_votes_count
        except ZeroDivisionError:
            return 0

    def __unicode__(self):
        return '{0} ({1})'.format(self.choice, self.votes)
