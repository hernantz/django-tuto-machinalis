from polls.models import Poll
from polls.forms import VoteForm, ChoiceInlineFormSet, PollEditForm
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import ListView
from django.contrib import messages
from django.views.generic.edit import FormView


index = ListView.as_view(
    queryset=Poll.objects.all()[:5],
    context_object_name='polls_list',
    template_name='polls/index.html')


class DetailView(FormView):
    """
    Serve and process votes for each poll
    """
    form_class = VoteForm
    http_method_names = ['get', 'post']
    template_name = 'polls/detail.html'

    def dispatch(self, request, *args, **kwargs):
        """
        Get the appropiate poll instance
        """
        self.poll = get_object_or_404(Poll, pk=kwargs.get('poll_id'))
        return super(DetailView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        """
        Add the poll object to the kwargs as needed to instatiate VoteForm
        """
        kwargs = super(DetailView, self).get_form_kwargs()
        kwargs.update({'poll': self.poll})
        return kwargs

    def get_context_data(self, **kwargs):
        """
        Add the current poll object to the context
        """
        kwargs.update({'poll': self.poll})
        return kwargs

    def form_valid(self, form):
        choice = form.cleaned_data['choice']
        choice.votes += 1
        choice.save()
        messages.add_message(self.request, messages.INFO,
                             'Your choice has been saved')
        return redirect('polls:detail', poll_id=self.poll.id)


detail = DetailView.as_view()


def edit(request, poll_id):
    """
    Lets the user edit the poll question and the choices & their display order
    """
    poll = get_object_or_404(Poll, pk=poll_id)
    form = PollEditForm(instance=poll)
    formset = ChoiceInlineFormSet(instance=poll)

    if request.method == "POST":
        form = PollEditForm(request.POST, instance=poll)
        formset = ChoiceInlineFormSet(request.POST, request.FILES,
                                      instance=poll)
        if form.is_valid() and formset.is_valid():
            form.save()
            formset.save()
            return redirect('polls:detail', poll_id=poll.pk)

    return render(request, 'polls/edit.html', {'poll': poll, 'form': form,
                                               'formset': formset})
