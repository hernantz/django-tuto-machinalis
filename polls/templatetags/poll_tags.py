from django import template
from polls.models import Choice


register = template.Library()


@register.inclusion_tag('polls/poll_vote_percentage_bar.html')
def votes_percentage_bar(choice_id):
    return {'choice': Choice.objects.get(pk=choice_id)}
