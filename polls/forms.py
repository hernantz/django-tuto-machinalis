from django import forms
from polls.models import Poll, Choice
from django.forms.models import inlineformset_factory, BaseInlineFormSet
from django.forms.widgets import HiddenInput


class VoteForm(forms.Form):
    choice = forms.ModelChoiceField(
        queryset=Choice.objects.all(),
        widget=forms.RadioSelect,
        empty_label=None

    )

    def __init__(self, *args, **kwargs):
        poll = kwargs.pop('poll')
        super(VoteForm, self).__init__(*args, **kwargs)
        self.fields['choice'].queryset = poll.choice_set.all()


class PollEditForm(forms.ModelForm):
    class Meta:
        model = Poll
        fields = ('question',)


class ChoiceEditForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ('choice',)


class ChoiceEditInlineFormset(BaseInlineFormSet):
    def get_queryset(self):
        """
        Redefine the queryset to retrieve all the poll's choices in order
        """
        self._queryset = self.instance.choice_set.order_by('_order')
        return self._queryset

    def add_fields(self, form, index):
        super(ChoiceEditInlineFormset, self).add_fields(form, index)

        # The order field must be rendered as a hidden input
        form.fields['ORDER'].widget = HiddenInput(attrs={'class': 'order'})

    def save(self, commit=True):
        """
        Update the choice's order on save
        """
        super(ChoiceEditInlineFormset, self).save(commit)

        order = []
        for form in self.ordered_forms:
            order.append(form.cleaned_data['id'].pk)

        self.instance.set_choice_order(order)


ChoiceInlineFormSet = inlineformset_factory(Poll, Choice, form=ChoiceEditForm,
                                            formset=ChoiceEditInlineFormset,
                                            can_order=True)
