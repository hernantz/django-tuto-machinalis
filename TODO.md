TODO
====

1) Add tests for polls
2) Add pagination to polls list
3) Add crispy forms app
4) Two column view of polls with wrap text
5) Add archive generic views and tests
6) Test edit formset with mocks 
7) Check was_published_recently tests
8) Add js new choice 
